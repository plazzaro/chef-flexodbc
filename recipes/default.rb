#
# Cookbook Name:: flexodbc
# Recipe:: default
#
# Copyright (C) 2014 YOUR_NAME
#
# All rights reserved - Do Not Redistribute
#

windows_package 'FlexODBC Driver 32' do
    source 'https://pureharvest.s3.amazonaws.com/files/public/odbc4.EXE'
    installer_type :wise
    action :install
end

registry_key "HKEY_LOCAL_MACHINE\\SOFTWARE\\FlexODBC" do
    architecture :i386
    values [{
            :name => "Region",
            :type => :string,
            :data => "O"
        },
        {
            :name => "Installation Directory",
            :type => :string,
            :data => "C:\\PROGRA~1\\FLEXOD~1"
        }
    ]
    action :create
end

