name             'flexodbc'
maintainer       'Pieter Lazzaro'
maintainer_email 'pieter.lazzaro@pureharvest.com.au'
license          'All rights reserved'
description      'Installs/Configures flexodbc'
long_description 'Installs/Configures flexodbc'

version          '0.1.0'

depends "windows"
