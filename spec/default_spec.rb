require 'spec_helper'

describe 'flexodbc::default' do

	context 'Windows 2012' do
		let(:runner) {
			ChefSpec::Runner.new(
				platform: 'windows', version: '2012'
			).converge('flexodbc::default')
		}

		it 'installs the flexodbc package' do 
			expect(runner).to install_windows_package(
				'FlexODBC Driver 32'
			).with(
				installer_type: :wise
			)
		end

		it 'creates a registry entry for the FlexODBC region' do
			expect(runner).to create_registry_key(
				"HKEY_LOCAL_MACHINE\\SOFTWARE\\FlexODBC"
				).with(
					architecture: :i386,
					values: [{
			            :name => "Region",
			            :type => :string,
			            :data => "O"
			        },
			        {
			            :name => "Installation Directory",
			            :type => :string,
			            :data => "C:\\PROGRA~1\\FLEXOD~1"
			        }]
				)
		end
	end
end